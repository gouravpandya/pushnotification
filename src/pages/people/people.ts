import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Events } from 'ionic-angular';
@Component({
  selector: 'page-people',
  templateUrl: 'people.html'
})
export class PeoplePage {
peopleArray=[];
carArray=[];
  constructor(public navCtrl: NavController,public events: Events) {
  	events.subscribe('peopleEvent', (people) => {
  		console.log(people)
  		if(people){
  			this.peopleArray.push(people);
		    localStorage.setItem('peopleData', JSON.stringify(this.peopleArray));
		    this.peopleArray = JSON.parse(localStorage.getItem("peopleData"));
		    console.log(this.peopleArray)
  		}
	    
	});
	events.subscribe('carEventOccur', (car) => {
  			this.carArray.push(car);
		    localStorage.setItem('carData', JSON.stringify(this.carArray));
		    this.carArray = JSON.parse(localStorage.getItem("carData"));
		    console.log(this.carArray)
	});

  }

  ionViewDidLoad() {
  	if(JSON.parse(localStorage.getItem("peopleData"))){
  		this.peopleArray = JSON.parse(localStorage.getItem("peopleData"));
  	}

  }



}
