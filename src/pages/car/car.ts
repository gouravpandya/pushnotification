import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Events } from 'ionic-angular';

@Component({
  selector: 'page-car',
  templateUrl: 'car.html'
})
export class CarPage {
carArray = [];
  constructor(public navCtrl: NavController,public events: Events) {
  	events.subscribe('carEventOccur', (car) => {
  			this.carArray.push(car);
		    localStorage.setItem('carData', JSON.stringify(this.carArray));
		    this.carArray = JSON.parse(localStorage.getItem("carData"));
		    console.log(this.carArray)
	});
  }

  ionViewDidLoad() {
  	if(JSON.parse(localStorage.getItem("carData"))){
  		this.carArray = JSON.parse(localStorage.getItem("carData"));
  	}
  	
  }

}
