import { Component } from '@angular/core';

import { CarPage } from '../car/car';
import { PeoplePage } from '../people/people';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = PeoplePage;
  tab2Root = CarPage;

  constructor() {

  }
}
