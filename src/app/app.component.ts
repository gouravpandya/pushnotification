import { Component } from '@angular/core';
import { Platform,ToastController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { FCM } from '@ionic-native/fcm';
import { TabsPage } from '../pages/tabs/tabs';
import { Device } from '@ionic-native/device';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Events } from 'ionic-angular';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = TabsPage;
 
  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen,private fcm: FCM,private device: Device,public toastCtrl: ToastController,public http: Http,public events: Events) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
      fcm.getToken().then(fcmtoken=>{
          if(fcmtoken){
            let url = 'http://divinewelfare.com/laravel/registration';
            let data = {
              token: fcmtoken,
              deviceType: this.device.platform,
            };
            this.http.post(url, data).map(res=>res.json()).subscribe(data=>{
              console.log(data)             
            }, err=>{
              console.log("Error!:", err);
            });
          }
      })

      fcm.onNotification().subscribe(data=>{
          if(data.types =="people"){
            this.presentToast('New Notification recieved people');
            this.events.publish('peopleEvent', data);  
          }else if(data.types =="cars"){
            this.presentToast('New Notification recieved car');
            this.events.publish('carEventOccur', data);
          }
      })

      


    });
  }

  public presentToast(msg:string) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position:'top'
    });
    toast.present();
  }
}
